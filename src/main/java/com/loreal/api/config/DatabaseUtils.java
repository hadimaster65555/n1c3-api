package com.loreal.api.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class DatabaseUtils implements EnvironmentAware {

    private Environment env;

    @Bean
    public JdbcTemplate timbanganJdbcTemplate(
            @Qualifier("databaseTimbangan") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
//
//    @Bean
//    public JdbcTemplate aplikasiJdbcTemplate(@Qualifier("datasouceApplication")DataSource dataSource){
//        return new JdbcTemplate(dataSource);
//    }
    
    @Bean
    public JdbcTemplate securityTemplate (@Qualifier("datasouceApplication") DataSource dataSource) {
    	return new JdbcTemplate(dataSource);
    }
    
    
    @Bean
    public NamedParameterJdbcTemplate timbanganNamedParameterJdbcTemplate(
            @Qualifier("databaseTimbangan") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate aplicationNamedParameterJdbcTemplate(
            @Qualifier("datasouceApplication") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    @Primary
    public DataSource databaseTimbangan() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(env.getProperty("timbangan.spring.datasource.url"));
        dataSource.setDriverClassName(env.getProperty("timbangan.spring.datasource.driver-class-name"));
        dataSource.setUsername(env.getProperty("timbangan.spring.datasource.username"));
        dataSource.setPassword(env.getProperty("timbangan.spring.datasource.password"));
        return dataSource;
    }

    @Bean
    public DataSource datasouceApplication() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(env.getProperty("aplikasi.spring.datasource.url"));
        dataSource.setDriverClassName(env.getProperty("aplikasi.spring.datasource.driver-class-name"));
        dataSource.setUsername(env.getProperty("aplikasi.spring.datasource.username"));
        dataSource.setPassword(env.getProperty("aplikasi.spring.datasource.password"));
        return dataSource;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.env = environment;
    }
}
