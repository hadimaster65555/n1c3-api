package com.loreal.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Idle {


    public Integer NoLinePesee;
    public String Equipment;
    public Integer Shift;
    public String LoginName;
    public Timestamp Tanggal;
    public Integer Type;
    public Timestamp Start;
    public Timestamp Stop;
    public String Reason;
    public String Status;

}
