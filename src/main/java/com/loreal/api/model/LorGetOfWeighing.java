package com.loreal.api.model;

public class LorGetOfWeighing {

	private Double wipOrderNo;
	private String productNo;
	private String medium;
//	private String department;
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "WIB")
//    private Timestamp createdOn;
	private String group;
	private String groupDesc;
	private Double orderQuantity;
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "WIB")
//    private Timestamp nombreLigneId;
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "WIB")
//    private Timestamp nombreLignePP;
//	private Float nombreLignePesee;
//	private Float progressStatus;
//	private String statusOF;
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "WIB")
//    private Timestamp findePesee;
	public LorGetOfWeighing(String productNo) {
		super();
		this.productNo = productNo;
	}
	
	
	
	public LorGetOfWeighing(Double wipOrderNo) {
	super();
	this.wipOrderNo = wipOrderNo;
	}



	public LorGetOfWeighing() {
	super();
	}

	public Double getWipOrderNo() {
		return wipOrderNo;
	}

	public void setWipOrderNo(Double wipOrderNo) {
		this.wipOrderNo = wipOrderNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public Double getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Double orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	
	
	
}
