package com.loreal.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LorGetMP {


    private Integer NoLinePeseé;
    private Double WipOrderNo;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "WIB")
    private Timestamp StartedOn;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "WIB")
    private Timestamp CompletedOn;
    private String Equipment;
    private String LoginName;
    private String Name;
    private String Status;
}
