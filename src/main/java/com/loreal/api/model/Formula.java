package com.loreal.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Formula {

	private Integer idBulk;
	private String formula;
	private String produk;
	private Double rmQuantity;
	
	
	public Formula() {
		super();
	}
	public Formula(String formula) {
		super();
		this.formula = formula;
	}
	public Integer getIdBulk() {
		return idBulk;
	}
	public void setIdBulk(Integer idBulk) {
		this.idBulk = idBulk;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	public String getProduk() {
		return produk;
	}
	public void setProduk(String produk) {
		this.produk = produk;
	}
	public Double getRmQuantity() {
		return rmQuantity;
	}
	public void setRmQuantity(Double rmQuantity) {
		this.rmQuantity = rmQuantity;
	}
	
	
}
