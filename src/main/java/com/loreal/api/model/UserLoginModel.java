package com.loreal.api.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginModel {

	public String nik;
	public Timestamp login;
	public Timestamp logout;
	public Integer shift;
}
