package com.loreal.api.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserOFModel {

	public String nik;
	public Timestamp tanggal;
	public String weighingBox;
	public Double ofNo;
	public String formula;
	public Double shift;
	public Double orderQuantity;
	public Double rmQuantity;
	public Timestamp endOFTime;
	
}
