package com.loreal.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BreakdownModel {

    public String LoginName;
    public String WipOrderNo;
    public Timestamp DateTime;
    public String PotType;
    public String Reason;
    public String Duration;
}
