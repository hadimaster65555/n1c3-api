package com.loreal.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Performance {

    private Double ofShift;
    private Double rmShift;
    private Double packShift;
    private Double efisiensi;

}
