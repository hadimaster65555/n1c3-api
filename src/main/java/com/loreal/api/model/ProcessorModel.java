package com.loreal.api.model;

public class ProcessorModel {

	private String nik;
	private String nama;
	private String password;
	private String roles;
	
	public ProcessorModel() {
		super();
	}

	public ProcessorModel(String nik) {
		super();
		this.nik = nik;
	}



	public String getNik() {
		return nik;
	}



	public void setNik(String nik) {
		this.nik = nik;
	}



	public String getNama() {
		return nama;
	}



	public void setNama(String nama) {
		this.nama = nama;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getRoles() {
		return roles;
	}



	public void setRoles(String roles) {
		this.roles = roles;
	}

	
	
}
