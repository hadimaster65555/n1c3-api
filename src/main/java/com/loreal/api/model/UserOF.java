package com.loreal.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserOF {

    public String NIK;
    public Timestamp Tanggal;
    public String WeighingBox;
    public Double OFNo;
    public String Formula;
    public Double Shift;
    public Double OrderQuantity;
    public Double RMQuantity;
    public Double OF_Quantity;
    public Double OF_RM;
    public Double OF_Pack;
    public Double OF_Achievement;
    public Timestamp EndOFTime;

}
