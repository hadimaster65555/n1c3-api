package com.loreal.api.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.loreal.api.model.WeighingBoxModel;

@Repository
public class WeighingBoxDaoImpl {

	@Autowired
	@Qualifier("aplicationNamedParameterJdbcTemplate")
	NamedParameterJdbcTemplate weighingBoxParameterJdbcTemplate;
	
	public List<WeighingBoxModel> getAllWeighingBox() {
		String sql = "SELECT * FROM WeighingBox";
		List<WeighingBoxModel> listWeighingBox = weighingBoxParameterJdbcTemplate.query(sql, getSqlParameterModel(null), new WeighingBoxMapper());
		return listWeighingBox;
	}
	
	private SqlParameterSource getSqlParameterModel(WeighingBoxModel weighingBoxModel) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(weighingBoxModel !=null) {
			paramSource.addValue("WeighingBox", weighingBoxModel.getWeighingBox());
			paramSource.addValue("AliasName", weighingBoxModel.getAliasName());
		}
		return paramSource;
	}

	private static final class WeighingBoxMapper implements RowMapper<WeighingBoxModel> {
		public WeighingBoxModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			WeighingBoxModel weighingBoxModel = new WeighingBoxModel();
			weighingBoxModel.setWeighingBox(rs.getString("WeighingBox"));
			weighingBoxModel.setAliasName(rs.getString("AliasName"));
			return weighingBoxModel;
		}
	}
}
