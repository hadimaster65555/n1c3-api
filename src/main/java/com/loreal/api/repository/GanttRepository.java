package com.loreal.api.repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.loreal.api.model.GanttChart;
import com.loreal.api.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true)
public class GanttRepository {

    private static final Logger console = LoggerFactory.getLogger(BreakdownRepository.class);

    @Autowired
    @Qualifier("timbanganNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate timbanganNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("aplicationNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate aplicationNamedParameterJdbcTemplate;

    @JsonFormat(pattern="yyyy-MM-dd")
    private static final LocalDate tanggal = LocalDate.now();

    public List<GanttChart> getGanttData(String nik){
        String query = "SELECT * FROM (SELECT [Type] AS [Description], [Start], [Stop] FROM UserWT  WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal UNION ALL SELECT P.[Description], U.[Start], U.[Stop] FROM UserPI U INNER JOIN ActionType A ON U.IDActionType = A.ID LEFT JOIN ActionType P ON A.ParentID = P.ID WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal) AS T1 ORDER BY [Start]";
        Map<String, Object> params = new HashMap<>();
        params.put("nik", nik);
        params.put("tanggal", tanggal);
        return this.aplicationNamedParameterJdbcTemplate.query(query, params, (resultSet, i) -> {
            GanttChart gantt = new GanttChart();
            gantt.setDescription(resultSet.getString("Description"));
            gantt.setStart(resultSet.getTimestamp("Start"));
            gantt.setStop(resultSet.getTimestamp("Stop"));
            console.info("{}", gantt);
            return gantt;
        });
    }
}
