package com.loreal.api.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.loreal.api.model.UserOFModel;

@Repository
public class UserOFRepository {

	@Autowired
	@Qualifier("aplicationNamedParameterJdbcTemplate")
	NamedParameterJdbcTemplate userOFParameterJdbcTemplate;
	
	public void insertAllDataUserOf(UserOFModel userOfModel) {
		String sql = "INSERT INTO UserOF (NIK,Tanggal,WeighingBox,OFNo,Formula,Shift,OrderQuantity,RMQuantity) VALUES (:nik,:tanggal,:weighingBox,:ofNo,:formula,:shift,:orderQuantity,:rmQuantity)";
		 Map<String, Object> params = new HashMap<>();
		 params.put("nik", userOfModel.nik);
		 params.put("tanggal", userOfModel.tanggal);
		 params.put("weighingBox", userOfModel.weighingBox);
		 params.put("ofNo", userOfModel.ofNo);
		 params.put("formula", userOfModel.formula);
		 params.put("shift", userOfModel.shift);
		 params.put("orderQuantity", userOfModel.orderQuantity);
		 params.put("rmQuantity", userOfModel.rmQuantity);
		 this.userOFParameterJdbcTemplate.update(sql, params);
	}
}
