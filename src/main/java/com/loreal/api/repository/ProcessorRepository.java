package com.loreal.api.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.loreal.api.model.ProcessorModel;

@Repository
public class ProcessorRepository {

	@Autowired
	@Qualifier("aplicationNamedParameterJdbcTemplate")
	NamedParameterJdbcTemplate processorParameterJdbcTemplate;
	
	@Autowired
	@Qualifier("securityTemplate")
	JdbcTemplate securityParameterJdbcTemplate;
	
//	@Autowired
//	public void setDatasource(DataSource dataSource) {
//		this.securityParameterJdbcTemplate = new JdbcTemplate(dataSource);
//	}
////	Security
//	public ProcessorModel getUserData(String nik) {
//		String sql = "SELECT NIK, Password, Roles FROM Processor WHERE NIK=?";
//		ProcessorModel processorModel = (ProcessorModel)securityParameterJdbcTemplate.queryForObject(sql, new RowMapper<ProcessorModel>() {
//			public ProcessorModel mapRow(ResultSet rs, int rowNum) throws SQLException {
//				ProcessorModel user = new ProcessorModel();
//				user.setNik(rs.getString("nik"));
//				user.setPassword(rs.getString("password"));
//				user.setRoles(rs.getString("roles"));
//				return user;
//			}
//		});
//		return processorModel;
//	}
	
//	Processor Data
	public List<ProcessorModel> getAllProcessor() {
		String sql = "SELECT * FROM Processor";
		List<ProcessorModel> listProcessor = processorParameterJdbcTemplate.query(sql, getSqlParameterModel(null), new ProcessorModelMapper());
		return listProcessor;
	}
	
	private SqlParameterSource getSqlParameterModel (ProcessorModel processorModel) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(processorModel != null) {
			paramSource.addValue("nik", processorModel.getNik());
			paramSource.addValue("nama", processorModel.getNama());
			paramSource.addValue("password", processorModel.getPassword());
		}
		return paramSource;
	}
	
	private static final class ProcessorModelMapper implements RowMapper<ProcessorModel> {
		public ProcessorModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			ProcessorModel processorModel = new ProcessorModel();
			processorModel.setNik(rs.getString("NIK"));
			processorModel.setNama(rs.getString("Nama"));
			processorModel.setPassword(rs.getString("Password"));
			return processorModel;
		}
	}
	
	public int getProcessorByUserPassword(String paramNIK, String paramPassword) {
		String sql = "SELECT COUNT(*) FROM Processor WHERE NIK = :nik AND Password = :password"; //0
                
                MapSqlParameterSource paramSource = new MapSqlParameterSource();
                paramSource.addValue("nik", paramNIK);
                paramSource.addValue("password", paramPassword);
                
		int result = processorParameterJdbcTemplate.queryForObject(sql, paramSource, Integer.class);
                return result;
	}
	
	public ProcessorModel getProcessorByUsername(String nik) {
		String sql = "SELECT * FROM Processor WHERE NIK = :nik";
		return processorParameterJdbcTemplate.queryForObject(sql, getSqlParameterModel(new ProcessorModel(nik)), new ProcessorModelMapper());
	}
	
	
}
