package com.loreal.api.repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.loreal.api.model.Idle;
import com.loreal.api.model.LorGetMP;
import com.loreal.api.model.UserOF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true)
public class StatusOFRepository {

    private static final Logger console = LoggerFactory.getLogger(BreakdownRepository.class);

    @Autowired
    @Qualifier("timbanganNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate timbanganNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("aplicationNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate aplicationNamedParameterJdbcTemplate;

    public Integer getRM(Double wiperOrderNo) {

        String query = "SELECT COUNT(DISTINCT(ProductNo)) AS RMQuantity FROM [Lor Get MP] WHERE WipOrderNo = :wipOrder";
        Map<String, Object> param = new HashMap<>();
        param.put("wipOrder", wiperOrderNo);

        Integer jumlahRM = this.timbanganNamedParameterJdbcTemplate.queryForObject(
            query,
            param,
            (resultSet, i) -> resultSet.getInt(1)
        );
        return jumlahRM;

    }

    public Double getWeigh(Double wiperOrderNo) {

        String query = "SELECT SUM(QuantitéPesée) AS TotalWeight FROM [Lor Get MP] WHERE WipOrderNo = :wipOrder";
        Map<String, Object> param = new HashMap<>();
        param.put("wipOrder", wiperOrderNo);

        Double weighBerjalan = this.timbanganNamedParameterJdbcTemplate.queryForObject(
            query,
            param,
            (resultSet, i) -> resultSet.getDouble(1)
        );
        return weighBerjalan;

    }

    public Integer getPack(Double wiperOrderNo) {

        String query = "SELECT COUNT(*) AS TotalPack FROM [Lor Get MP] WHERE WipOrderNo = :wipOrder";
        Map<String, Object> param = new HashMap<>();
        param.put("wipOrder", wiperOrderNo);

        Integer weighBerjalan = this.timbanganNamedParameterJdbcTemplate.queryForObject(
                query,
                param,
                (resultSet, i) -> resultSet.getInt(1)
        );
        return weighBerjalan;

    }

    public Double getQuantity(Double wiperOrderNo) {

        String query = "SELECT OrderQuantity FROM [Lor Get OF Weighing] WHERE WipOrderNo = :wipOrder";
        Map<String, Object> param = new HashMap<>();
        param.put("wipOrder", wiperOrderNo);

        Double weighBerjalan = this.timbanganNamedParameterJdbcTemplate.queryForObject(
                query,
                param,
                (resultSet, i) -> resultSet.getDouble(1)
        );
        return weighBerjalan;

    }

    public Double getRMQuantity(Double wiperOrderNo) {

        String query = "SELECT OrderQuantity FROM [Lor Get OF Weighing] WHERE WipOrderNo = :wipOrder";
        Map<String, Object> param = new HashMap<>();
        param.put("wipOrder", wiperOrderNo);

        Double weighBerjalan = this.timbanganNamedParameterJdbcTemplate.queryForObject(
                query,
                param,
                (resultSet, i) -> resultSet.getDouble(1)
        );
        return weighBerjalan;

    }

    @JsonFormat(pattern="yyyy-MM-dd")
    private static final LocalDate date = LocalDate.now();

    public void updateUserOF(UserOF userOF) {
        String sql = "UPDATE UserOF SET OF_Quantity = :weigh, OF_RM = :rm, OF_Pack = :pack WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal";
        // Save Data
        Map<String, Object> params = new HashMap<>();
        params.put("weigh", userOF.OF_Quantity);
        params.put("rm", userOF.OF_RM);
        params.put("pack", userOF.OF_Pack);
        params.put("nik", userOF.NIK);
        params.put("tanggal", date);
        this.aplicationNamedParameterJdbcTemplate.update(sql, params);

        String sqlOF= "UPDATE UserOF SET OF_Achievement = ((CAST(OF_Quantity AS FLOAT)-CAST((SELECT ISNULL(SUM(OF_Quantity), 0) FROM UserOF WHERE OFNo = :of AND Formula = :formula AND (NIK <> :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') <> :tanggal AND [Shift] <> :shift)) AS FLOAT))/CAST(OrderQuantity AS FLOAT)) WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal AND WeighingBox = :weighing AND OFNo = :of AND Formula = :formula AND Shift = :shift";
        Map<String, Object> paramsOF = new HashMap<>();
        paramsOF.put("of", userOF.OFNo);
        paramsOF.put("formula", userOF.Formula);
        paramsOF.put("weighing", userOF.WeighingBox);
        paramsOF.put("shift", userOF.Shift);
        paramsOF.put("nik", userOF.NIK);
        paramsOF.put("tanggal", date);
        this.aplicationNamedParameterJdbcTemplate.update(sqlOF, paramsOF);
    }

    public void stopEndOf(UserOF userOF) {
        String sql = "UPDATE UserOF SET OF_Quantity = :weigh, OF_RM = :rm, OF_Pack = :pack EndOFTime = :endOf WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal";
        // Save Data
        Map<String, Object> params = new HashMap<>();
        params.put("weigh", userOF.OF_Quantity);
        params.put("rm", userOF.OF_RM);
        params.put("pack", userOF.OF_Pack);
        params.put("nik", userOF.NIK);
        params.put("tanggal", date);
        params.put("endOf", userOF.EndOFTime);
        this.aplicationNamedParameterJdbcTemplate.update(sql, params);
    }

    public Double getIndividualAchievement(String nik) {

        String query = "SELECT CAST(SUM(ISNULL(OF_Achievement, 0)) AS FLOAT) / CAST(DAY(GETDATE()) AS FLOAT)  FROM UserOF WHERE NIK = :nik AND Tanggal BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND GETDATE()";
        Map<String, Object> param = new HashMap<>();
        param.put("nik", nik);

        Double individualAchievement = this.aplicationNamedParameterJdbcTemplate.queryForObject(
                query,
                param,
                (resultSet, i) -> resultSet.getDouble(1)
        );
        return individualAchievement;

    }

    public Double getGroupAchievement() {

        String query = "SELECT CAST(SUM(ISNULL(OF_Achievement, 0)) AS FLOAT) / CAST(DAY(GETDATE()) AS FLOAT)  FROM UserOF WHERE Tanggal BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND GETDATE()";
        Map<String, Object> param = new HashMap<>();

        Double groupAchievement = this.aplicationNamedParameterJdbcTemplate.queryForObject(
                query,
                new HashMap<>(),
                (resultSet, i) -> resultSet.getDouble(1)
        );
        return groupAchievement;

    }

}
