package com.loreal.api.repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.loreal.api.model.Idle;
import com.loreal.api.model.LorGetMP;
import com.loreal.api.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true)
public class BreakdownRepository {

    private static final Logger console = LoggerFactory.getLogger(BreakdownRepository.class);

    @Autowired
    @Qualifier("timbanganNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate timbanganNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("aplicationNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate aplicationNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("timbanganJdbcTemplate")
    private JdbcTemplate timbanganJdbcTemplate;

    @JsonFormat(pattern="yyyy-MM-dd")
    private static final LocalDate tanggal = LocalDate.now();



    public List<LorGetMP> getMp() {
        String query = "SELECT * from [Lor Get MP] ORDER BY StartedOn DESC";
        return this.timbanganNamedParameterJdbcTemplate.query(query, (resultSet, i) -> {
            LorGetMP lorGetMP = new LorGetMP();
            lorGetMP.setStartedOn(resultSet.getTimestamp("StartedOn"));
            lorGetMP.setCompletedOn(resultSet.getTimestamp("CompletedOn"));
            lorGetMP.setLoginName(resultSet.getString("LoginName"));
            lorGetMP.setWipOrderNo(resultSet.getDouble("WipOrderNo"));
            return lorGetMP;
        });
    }


    public List<Type> getType(){
        String queryType = "SELECT * FROM ActionType";
        return this.aplicationNamedParameterJdbcTemplate.query(queryType, (resultSet, i) -> {
            Type type = new Type();
            type.setID(resultSet.getInt("ID"));
            type.setDescription(resultSet.getString("Description"));
            type.setParentID(resultSet.getInt("ParentID"));
            console.info("{}", type);
            return type;
        });
    }

    public List<Type> getLevelDua(Integer parent){
        String queryLevel = "SELECT * FROM ActionType WHERE ParentID = :parent";
        Map<String, Object> param = new HashMap<>();
        param.put("parent", parent);
        return this.aplicationNamedParameterJdbcTemplate.query(queryLevel, param, (resultSet, i) -> {
            Type type = new Type();
            type.setID(resultSet.getInt("ID"));
            type.setDescription(resultSet.getString("Description"));
            type.setParentID(resultSet.getInt("ParentID"));
            console.info("{}", type);
            return type;
        });
    }


    public List<LorGetMP> getIdle(String nik) {

        String queryTime = "SELECT Value from [Settings] WHERE ID = 1";

        Integer waktu = this.aplicationNamedParameterJdbcTemplate.queryForObject(
                queryTime,
                new HashMap<>(),
                (resultSet, i) -> resultSet.getInt(1)
        );

        String query2 = "SELECT TOP 1 * from [Lor Get MP] WHERE LoginName = :nik AND FORMAT(StartedOn, 'yyyy-MM-dd') = :tanggal ORDER BY StartedOn DESC";
        Map<String, Object> param = new HashMap<>();
        param.put("nik", nik);
        param.put("tanggal",tanggal);

        try {
            return this.timbanganNamedParameterJdbcTemplate.query(query2, param, (resultSet, i) -> {
                LocalDateTime date = LocalDateTime.now();

                long selisih = ChronoUnit.MINUTES.between(resultSet.getTimestamp("CompletedOn").toLocalDateTime(), date);
                console.info("selisih waktu {}:{}", selisih, waktu);
                if (selisih >= waktu) {
                    LorGetMP lorGetMP = new LorGetMP();
                    lorGetMP.setNoLinePeseé(resultSet.getInt("NoLinePeseé"));
                    lorGetMP.setWipOrderNo(resultSet.getDouble("WipOrderNo"));
                    lorGetMP.setStartedOn(resultSet.getTimestamp("StartedOn"));
                    lorGetMP.setCompletedOn(resultSet.getTimestamp("CompletedOn"));
                    lorGetMP.setLoginName(resultSet.getString("LoginName"));
                    lorGetMP.setName(resultSet.getString("Name"));
                    lorGetMP.setEquipment(resultSet.getString("Equipment"));
                    lorGetMP.setStatus("IDLE");
                    console.info("{}", lorGetMP);
                    return lorGetMP;

                } else {
                    LorGetMP lorGetMP = new LorGetMP();
                    lorGetMP.setNoLinePeseé(resultSet.getInt("NoLinePeseé"));
                    lorGetMP.setWipOrderNo(resultSet.getDouble("WipOrderNo"));
                    lorGetMP.setStartedOn(resultSet.getTimestamp("StartedOn"));
                    lorGetMP.setCompletedOn(resultSet.getTimestamp("CompletedOn"));
                    lorGetMP.setLoginName(resultSet.getString("LoginName"));
                    lorGetMP.setName(resultSet.getString("Name"));
                    lorGetMP.setEquipment(resultSet.getString("Equipment"));
                    lorGetMP.setStatus("AMAN");
                    console.info("{}", lorGetMP);
                    return lorGetMP;
                }

            });
        } catch (DataAccessException e) {
            e.printStackTrace();
            console.error(e.getMessage());
            return new ArrayList<>();
        }
    }

//    Date.valueOf(LocalDate.now())

    public void insert(Idle idle) {
        String sql = "INSERT INTO UserPI (NoLinePesee, WeighingBox, Nik, Tanggal, IDActionType, Start, Stop, Reason) VALUES (:NoLinePesee, :Equipment, :Nik, :Tanggal, :ActionType, :Start, :Stop, :Reason)";
        // Save Data
        Map<String, Object> params = new HashMap<>();
        params.put("NoLinePesee", idle.NoLinePesee);
        params.put("Equipment", idle.Equipment);
        params.put("Nik", idle.LoginName);
        params.put("Tanggal", idle.Tanggal);
        params.put("ActionType", idle.Type);
        params.put("Start", idle.Start);
        params.put("Stop", idle.Stop);
        params.put("Reason", idle.Reason);
        this.aplicationNamedParameterJdbcTemplate.update(sql, params);
    }
}
