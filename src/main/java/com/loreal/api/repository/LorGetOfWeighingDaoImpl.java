package com.loreal.api.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.endpoint.jmx.DataEndpointMBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.loreal.api.model.LorGetOfWeighing;

@Repository
public class LorGetOfWeighingDaoImpl {

	@Autowired
	@Qualifier("timbanganNamedParameterJdbcTemplate")
	NamedParameterJdbcTemplate lorGetOfWeighingParameterJdbcTemplate;
	
	private final static Logger console = LoggerFactory.getLogger(LorGetOfWeighingDaoImpl.class);
	
//	Get All Formula and OF	(Start)
	public List<LorGetOfWeighing> getAllFormulaAndOF() {
		String sql = "SELECT DISTINCT WipOrderNo,Productno FROM [Lor Get OF Weighing]";
		List<LorGetOfWeighing> listAllFormulaAndOf = lorGetOfWeighingParameterJdbcTemplate.query(sql, getSqlParameterModelFormulaAndOF(null), new RowMapper<LorGetOfWeighing>() {

			@Override
			public LorGetOfWeighing mapRow(ResultSet rs, int arg1) throws SQLException {
				LorGetOfWeighing lorGetOfWeighing = new LorGetOfWeighing();
				lorGetOfWeighing.setWipOrderNo(rs.getDouble("WipOrderNo"));
				lorGetOfWeighing.setProductNo(rs.getString("Productno"));
				return lorGetOfWeighing;
			}
		});
		return listAllFormulaAndOf;
	}

	private SqlParameterSource getSqlParameterModelFormulaAndOF(LorGetOfWeighing lorGetOfWeighing) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(lorGetOfWeighing!=null) {
			paramSource.addValue("wipOrderNo", lorGetOfWeighing.getWipOrderNo());
			paramSource.addValue("productNo", lorGetOfWeighing.getProductNo());
		}
		return paramSource;
	}
//	Get All Formula and OF (End)
	
	private SqlParameterSource getSqlParameterModelOrderQuantityByFormula(LorGetOfWeighing lorGetOfWeighing) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(lorGetOfWeighing!=null) {
			paramSource.addValue("productNo", lorGetOfWeighing.getProductNo());
			paramSource.addValue("orderQuantity", lorGetOfWeighing.getOrderQuantity());
		}
		
		return paramSource;
	}
	
	private static final class getOrderQuantityByFormulaMapper implements RowMapper<LorGetOfWeighing> {
		public LorGetOfWeighing mapRow(ResultSet rs, int rowNum) throws SQLException {
			LorGetOfWeighing lorGetOfWeighing = new LorGetOfWeighing();
			lorGetOfWeighing.setProductNo(rs.getString("Productno"));
			lorGetOfWeighing.setOrderQuantity(rs.getDouble("OrderQuantity"));
			return lorGetOfWeighing;
		}
	}
	
	public LorGetOfWeighing getOrderQuantityByFormula(String productNo) throws EmptyResultDataAccessException {
		String sql = "SELECT OrderQuantity FROM [Lor Get OF Weighing] WHERE Productno = :productNo";
		Map<String, Object> params = new HashMap<>();
		params.put("productNo", productNo);
		return lorGetOfWeighingParameterJdbcTemplate.queryForObject(sql,params, new RowMapper<LorGetOfWeighing>() {

			@Override
			public LorGetOfWeighing mapRow(ResultSet rs, int arg1) throws SQLException {
				LorGetOfWeighing lorGetOfWeighing = new LorGetOfWeighing();
				lorGetOfWeighing.setOrderQuantity(rs.getDouble("OrderQuantity"));
				return lorGetOfWeighing;
			}
		});
	}
	
//	Get Formula by OF Number (Start)
	public LorGetOfWeighing getFormulaByOfNumber(Double wipOrderNo) {
		String sql = "SELECT DISTINCT WipOrderNo,Productno FROM [Lor Get OF Weighing] WHERE WipOrderNo = :wipOrderNo";
		return lorGetOfWeighingParameterJdbcTemplate.queryForObject(sql, getSqlParameterModelFormulaAndOF(new LorGetOfWeighing(wipOrderNo)), new getFormulaByOfNoMapper());
	}
	
	private static final class getFormulaByOfNoMapper implements RowMapper<LorGetOfWeighing> {
		public LorGetOfWeighing mapRow(ResultSet rs, int arg1) throws SQLException {
			LorGetOfWeighing lorGetOfWeighing = new LorGetOfWeighing();
			lorGetOfWeighing.setWipOrderNo(rs.getDouble("WipOrderNo"));
			lorGetOfWeighing.setProductNo(rs.getString("Productno"));
			return lorGetOfWeighing;
		}
	}
	

}
