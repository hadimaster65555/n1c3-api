package com.loreal.api.repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.loreal.api.model.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true)
public class PieRepository {

    private static final Logger console = LoggerFactory.getLogger(BreakdownRepository.class);

    @Autowired
    @Qualifier("timbanganNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate timbanganNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("aplicationNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate aplicationNamedParameterJdbcTemplate;

    @JsonFormat(pattern="yyyy-MM-dd")
    private static final LocalDate tanggal = LocalDate.now();

    public List<PieChartModel> getPieData(String nik, Integer shift){
        String query = "SELECT [Description], SUM (CAST(DATEDIFF(second, [Start], [Stop]) AS FLOAT)/CAST(60 AS FLOAT))/(SELECT CASE WHEN StartShift < EndShift THEN DATEDIFF(minute, StartShift, EndShift) ELSE DATEDIFF(minute, CAST(CAST('01/01/1900' AS VARCHAR(10)) + ' ' + CAST(StartShift AS VARCHAR(12)) AS DATETIME), CAST(CAST('01/02/1900' AS VARCHAR(10)) + ' ' + CAST(EndShift AS VARCHAR(12)) AS DATETIME)) END FROM ShiftTime WHERE ID = :shift) AS TotalUsed FROM (SELECT [Type] AS [Description],[Start],[Stop] FROM UserWT  WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal UNION ALL SELECT P.[Description],U.[Start],U.[Stop] FROM UserPI U INNER JOIN ActionType A ON U.IDActionType = A.ID LEFT JOIN ActionType P ON A.ParentID = P.ID WHERE NIK = :nik AND FORMAT(Tanggal, 'yyyy-MM-dd') = :tanggal) AS T1 GROUP BY [Description]";
        Map<String, Object> params = new HashMap<>();
        params.put("nik", nik);
        params.put("shift", shift);
        params.put("tanggal", tanggal);
        return this.aplicationNamedParameterJdbcTemplate.query(query, params, (resultSet, i) -> {
             PieChartModel pieChartModel = new PieChartModel();
             pieChartModel.setDescription(resultSet.getString("Description"));
             pieChartModel.setTotal(resultSet.getDouble("TotalUsed"));
             console.info("{}", pieChartModel);
             return pieChartModel;
        });

    }
}
