package com.loreal.api.repository;

import com.loreal.api.model.Performance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional(readOnly = true)
public class PerformanceRepository {

    private static final Logger console = LoggerFactory.getLogger(BreakdownRepository.class);

    @Autowired
    @Qualifier("timbanganNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate timbanganNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("aplicationNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate aplicationNamedParameterJdbcTemplate;

    public Performance getPerformanceShift(Integer shif) {

        String query = "SELECT ISNULL(SUM(OF_Achievement), 0) AS [OF], ISNULL(SUM(OF_RM), 0) AS [RM], ISNULL(SUM(OF_Pack), 0) AS [Pack], ((SELECT ISNULL(SUM(Total), 0) FROM UserWTTotal WHERE FORMAT(Tanggal, 'yyyy-MM-dd') = FORMAT(GETDATE(), 'yyyy-MM-dd') AND UPPER([Type]) = 'NETWT' AND NIK IN (SELECT O.NIK FROM UserWT U INNER JOIN UserOF O ON U.NIK = O.NIK AND U.WeighingBox = O.WeighingBox WHERE O.[Shift] = :shifKe AND FORMAT(O.Tanggal, 'yyyy-MM-dd') = FORMAT(GETDATE(), 'yyyy-MM-dd') )) / (SELECT CASE WHEN StartShift < EndShift THEN DATEDIFF(minute, StartShift, EndShift) ELSE DATEDIFF(minute, CAST(CAST('01/01/1900' AS VARCHAR(10)) + ' ' + CAST(StartShift AS VARCHAR(12)) AS DATETIME), CAST(CAST('01/02/1900' AS VARCHAR(10)) + ' ' + CAST(EndShift AS VARCHAR(12)) AS DATETIME)) END FROM ShiftTime WHERE ID = :shifKe)) AS Eficiency FROM UserOF WHERE [Shift] = :shifKe AND FORMAT(Tanggal, 'yyyy-MM-dd') = FORMAT(GETDATE(), 'yyyy-MM-dd')";
        Map<String, Object> params = new HashMap<>();
        params.put("shifKe", shif);

        return this.aplicationNamedParameterJdbcTemplate.queryForObject(query, params, (resultSet, i) -> {
            Performance performance = new Performance();
            performance.setOfShift(resultSet.getDouble("OF"));
            performance.setPackShift(resultSet.getDouble("RM"));
            performance.setRmShift(resultSet.getDouble("Pack"));
            performance.setEfisiensi(resultSet.getDouble("Eficiency"));
            return performance;
        });
    }


//    public List<Performance> getPerformanceShift1() {
//        String query = "SELECT ISNULL(SUM(OF_Achievement), 0) AS [OF], ISNULL(SUM(OF_RM), 0) AS [RM], ISNULL(SUM(OF_Pack), 0) AS [Pack] FROM UserOF WHERE [Shift] = 1 AND FORMAT(Tanggal, 'yyyy-MM-dd') = FORMAT(GETDATE(), 'yyyy-MM-dd')";
//        return this.aplicationNamedParameterJdbcTemplate.query(query, (resultSet, i) -> {
//            Performance performance = new Performance();
//            performance.setOfShift(resultSet.getDouble("OF"));
//            performance.setPackShift(resultSet.getDouble("RM"));
//            performance.setRmShift(resultSet.getDouble("Pack"));
//            return performance;
//        });
//    }
//
//    public List<Performance> getPerformanceShift2() {
//        String query = "SELECT ISNULL(SUM(OF_Achievement), 0) AS [OF], ISNULL(SUM(OF_RM), 0) AS [RM], ISNULL(SUM(OF_Pack), 0) AS [Pack] FROM UserOF WHERE [Shift] = 2 AND FORMAT(Tanggal, 'yyyy-MM-dd') = FORMAT(GETDATE(), 'yyyy-MM-dd')";
//        return this.aplicationNamedParameterJdbcTemplate.query(query, (resultSet, i) -> {
//            Performance performance = new Performance();
//            performance.setOFshift2(resultSet.getDouble("OF"));
//            performance.setPACKshift2(resultSet.getDouble("RM"));
//            performance.setRMshift2(resultSet.getDouble("Pack"));
//            return performance;
//        });
//    }
//
//    public List<Performance> getPerformanceShift3() {
//        String query = "SELECT ISNULL(SUM(OF_Achievement), 0) AS [OF], ISNULL(SUM(OF_RM), 0) AS [RM], ISNULL(SUM(OF_Pack), 0) AS [Pack] FROM UserOF WHERE [Shift] = 3 AND FORMAT(Tanggal, 'yyyy-MM-dd') = FORMAT(GETDATE(), 'yyyy-MM-dd')";
//        return this.aplicationNamedParameterJdbcTemplate.query(query, (resultSet, i) -> {
//            Performance performance = new Performance();
//            performance.setOFshift3(resultSet.getDouble("OF"));
//            performance.setPACKshift3(resultSet.getDouble("RM"));
//            performance.setRMshift3(resultSet.getDouble("Pack"));
//            return performance;
//        });
//    }

}
