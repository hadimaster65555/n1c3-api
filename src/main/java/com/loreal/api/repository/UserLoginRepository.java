package com.loreal.api.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.loreal.api.model.ProcessorModel;
import com.loreal.api.model.UserLoginModel;

@Repository
public class UserLoginRepository {

	@Autowired
	@Qualifier("aplicationNamedParameterJdbcTemplate")
	NamedParameterJdbcTemplate userLoginNamedParameterJdbcTemplate;
	
	public void insertLoginLogoutData(UserLoginModel userLoginModel) {
		String sql = "INSERT INTO UserLogin (NIK,Login,Logout,Shift) VALUES (:nik,:login,:logout,:shift)";
		Map<String, Object> params = new HashMap<>();
		params.put("nik", userLoginModel.nik);
		params.put("login", userLoginModel.login);
		params.put("logout", userLoginModel.logout);
		params.put("shift", userLoginModel.shift);
		this.userLoginNamedParameterJdbcTemplate.update(sql, params);
	}
	
//	private SqlParameterSource getSqlParameterModelLoginChecker (ProcessorModel processorModel) {
//		MapSqlParameterSource param = new MapSqlParameterSource();
//		if(processorModel !=null) {
//			param.addValue("nik", processorModel.getNik());
//			param.addValue("password", processorModel.getPassword());
//		}
//		return param;
//	}
//	
//	private static final class LoginCheckerMapper implements RowMapper<UserLoginModel> {
//		public UserLoginModel mapRow(ResultSet rs, int rowNum) throws SQLException {
//			UserLoginModel loginCheckerModel = new UserLoginModel();
//			loginCheckerModel.setNik(rs.getString("NIK"));
//			loginCheckerModel.setLogin(rs.getTimestamp("Login"));
//			loginCheckerModel.setLogout(rs.getTimestamp("Logout"));
//			loginCheckerModel.setShift(rs.getInt("Shift"));
//			return loginCheckerModel;
//		}
//	}
//	
//	public ProcessorModel LoginChecker(String nik, String password) {
//		String sql = "SELECT COUNT(*) FROM UserLogin WHERE NIK = :nik AND Password = :password";
//		return userLoginNamedParameterJdbcTemplate.queryForObject(sql, getSqlParameterModelLoginChecker(new ProcessorModel(nik,password)), new LoginCheckerMapper());
//	}
}
