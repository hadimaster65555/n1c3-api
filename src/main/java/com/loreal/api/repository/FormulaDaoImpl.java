package com.loreal.api.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.loreal.api.model.Formula;

@Repository
public class FormulaDaoImpl {
	
	@Autowired
	@Qualifier("aplicationNamedParameterJdbcTemplate")
	NamedParameterJdbcTemplate formulaParameterJdbcTemplate;
		
//	Get All Formula (Start)
	public List<Formula> getAllFormula() {
		String sql = "SELECT * FROM Formula";
		List<Formula> listFormula = formulaParameterJdbcTemplate.query(sql, getSqlParameterByModel(null), new FormulaMapper() );
		return listFormula;
	}
	
	
	private SqlParameterSource getSqlParameterByModel (Formula formula) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(formula != null) {
			paramSource.addValue("idBulk", formula.getIdBulk());
			paramSource.addValue("formula", formula.getFormula());
			paramSource.addValue("produk", formula.getProduk());
			paramSource.addValue("rmQuantity", formula.getRmQuantity());
		}
		return paramSource;
	}

	private static final class FormulaMapper implements RowMapper<Formula> {
		public Formula mapRow(ResultSet rs, int rowNum) throws SQLException {
			Formula formula = new Formula();
			formula.setIdBulk(rs.getInt("IDBulk"));
			formula.setFormula(rs.getString("Formula"));
			formula.setProduk(rs.getString("Produk"));
			formula.setRmQuantity(rs.getDouble("RMQuantity"));
			return formula;	
		}
	}
	
//	Get All Formula (End)
	
//	Get RMQuantity By Formula (Start)
	public Formula getRmQuantityByFormula(String formula) {
		String sql = "SELECT RMQuantity FROM Formula WHERE Formula = :formula";
		return formulaParameterJdbcTemplate.queryForObject(sql, getSqlParameterModelRmQuantityByFormula(new Formula(formula)), new RmQuantityByFormulaMapper());
	}
	
	private SqlParameterSource getSqlParameterModelRmQuantityByFormula(Formula formula) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(formula != null) {
			paramSource.addValue("formula", formula.getFormula());
		}
		return paramSource;
	}
	
	private static final class RmQuantityByFormulaMapper implements RowMapper<Formula> {
		public Formula mapRow(ResultSet rs, int rowNum) throws SQLException {
			Formula formula = new Formula();
			formula.setRmQuantity(rs.getDouble("RMQuantity"));
			return formula;	
		}
	}
//	Get RMQuantity By Formula (End)
	
}
