package com.loreal.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.loreal.api.api.Breakdown;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

@Component
public class JobSchedule {

    @Autowired
    private Breakdown breakdown;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "MM/dd/yyyy HH:mm:ss");

    @JsonFormat(pattern="yyyy-MM-dd")
    private static final LocalDate date = LocalDate.now();

    @Scheduled(fixedRate = 1000)
    public void performTask() {
//        System.out.println("Regular task performed at "
//                + dateFormat.format(new Date()));

//        List<LorGetMP> lor = breakdown.getIdle("UPSCLE07");
//        lor.forEach(b -> {
//            System.out.println(b.getNoLinePeseé()+" | "+b.getWipOrderNo() + " | "+b.getStartedOn().toString() + " | " + b.getCompletedOn().toString() + " | " + b.getStatus().toString() + dateFormat.format(new Date()) + "");
//        });

        //resultSet.getTimestamp("CompletedOn").toLocalDateTime(),
//        long waktu = 5;
//
//        long selisih = ChronoUnit.MINUTES.between(date,);
////        console.info("selisih waktu {}:{}", selisih, waktu);
//        System.out.println("selisih waktu {}:{}"+ selisih);

//        LocalDateTime date = LocalDateTime.now();
//        System.out.println(date);

    }

//    @Scheduled(initialDelay = 1000, fixedRate = 10000)
//    public void performDelayedTask() {
//
//        System.out.println("Delayed Regular task performed at "
//                + dateFormat.format(new Date()));
//
//    }
//
//    @Scheduled(cron = "1 * * * * *")
//    public void performTaskUsingCron() {
//
//        System.out.println("Regular task performed using Cron at "
//                + dateFormat.format(new Date()));
//
//    }

}