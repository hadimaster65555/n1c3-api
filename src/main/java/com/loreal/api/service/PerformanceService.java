package com.loreal.api.service;

import com.loreal.api.model.Performance;
import com.loreal.api.repository.PerformanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PerformanceService {

    @Autowired
    private PerformanceRepository repository;

    public Map<String, Performance> getPerformance() {
        Map<String, Performance> performances = new HashMap<>();

        Performance shift1 = this.repository.getPerformanceShift(1);
        Performance shift2 = this.repository.getPerformanceShift(2);
        Performance shift3 = this.repository.getPerformanceShift(3);

        performances.put("shift1", shift1);
        performances.put("shift2", shift2);
        performances.put("shift3", shift3);
        return performances;

    }
}
