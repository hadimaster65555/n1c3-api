package com.loreal.api.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loreal.api.model.WeighingBoxModel;
import com.loreal.api.repository.WeighingBoxDaoImpl;

@RestController
@RequestMapping("/rest")
public class WeighingBoxController {

	@Autowired
	WeighingBoxDaoImpl weighingBoxDaoImpl;
	
	@RequestMapping(value="/weighingbox/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
	public List<WeighingBoxModel> listWeighingBox() {
		return weighingBoxDaoImpl.getAllWeighingBox();
	}
}
