package com.loreal.api.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loreal.api.model.ProcessorModel;
import com.loreal.api.repository.ProcessorRepository;

@RestController
@RequestMapping("/rest")
public class ProcessorController {

	@Autowired
	ProcessorRepository processorRepository;
	
	@RequestMapping(value="/processor/{nik}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method= RequestMethod.GET)
	public ProcessorModel getAllProcessor(@PathVariable("nik") String nik) {
		return processorRepository.getProcessorByUsername(nik);
	}
	
	@RequestMapping(value="/processor/{nik}/{password}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method= RequestMethod.GET)
	public int getProcessorByUserPassword(@PathVariable("nik") String nik, @PathVariable String password) {
		return processorRepository.getProcessorByUserPassword(nik, password);
	}
}
