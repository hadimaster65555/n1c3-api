package com.loreal.api.api;

import com.loreal.api.model.Performance;
import com.loreal.api.repository.PerformanceRepository;
import com.loreal.api.service.PerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest")
public class PerformanceController {


    @Autowired
    private PerformanceService service;

    @Autowired
    private PerformanceRepository performanceRepository;

//    @GetMapping(value = "/performance1",
//            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public List<Performance> getPerformance1() {
//        return performanceRepository.getPerformanceShift1();
//    }
//
//    @GetMapping(value = "/performance2",
//            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public List<Performance> getPerformance2() {
//        return performanceRepository.getPerformanceShift2();
//    }
//
//    @GetMapping(value = "/performance3",
//            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public List<Performance> getPerformance3() {
//        return performanceRepository.getPerformanceShift3();
//    }


//    @GetMapping(value = "/performance",
//            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public String getAchievement() {
//        String value = "{\n" +
//                "  \"performanceShift1\" :\n" +
//                "" + performanceRepository.getPerformanceShift1() +
//                ",\n" +
//                "  \"performanceShift2\" :\n" +
//                "" + performanceRepository.getPerformanceShift2() +
//                ",\n" +
//                "  \"performanceShift3\" :\n" +
//                "" + performanceRepository.getPerformanceShift3() +
//                "}";
//        return value;
//    }

    @GetMapping(value = "/performances", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Map<String, Performance> getPerformances() {
        return service.getPerformance();
    }

}
