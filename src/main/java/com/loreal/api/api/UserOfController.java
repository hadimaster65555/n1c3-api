package com.loreal.api.api;

import com.loreal.api.model.UserOF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loreal.api.model.ReturnCode;
import com.loreal.api.model.UserOFModel;
import com.loreal.api.repository.UserOFRepository;

@RestController
@RequestMapping("/rest")
public class UserOfController {

	@Autowired
	UserOFRepository userOfRepository;

	@RequestMapping(value="/userOf/add", consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
	public ReturnCode postUserOfData(@RequestBody UserOFModel userOfModel) {
		userOfRepository.insertAllDataUserOf(userOfModel);
		return new ReturnCode(0,"Success");
	}
}
