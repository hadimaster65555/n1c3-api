package com.loreal.api.api;

import com.loreal.api.model.GanttChart;
import com.loreal.api.model.Type;
import com.loreal.api.repository.GanttRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class Gantt {

    @Autowired
    private GanttRepository ganttRepository;


    @RequestMapping(value = "/gantt/{nik}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<GanttChart> getGanttData(@PathVariable(name = "nik") String nik) {
        return ganttRepository.getGanttData(nik);
    }

}
