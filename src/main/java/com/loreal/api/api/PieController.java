package com.loreal.api.api;

import com.loreal.api.model.GanttChart;
import com.loreal.api.model.PieChartModel;
import com.loreal.api.repository.GanttRepository;
import com.loreal.api.repository.PieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class PieController {

    @Autowired
    private PieRepository pieRepository;

    @RequestMapping(value = "/pie/{nik}/{shift}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PieChartModel> getGanttData(@PathVariable(name = "nik") String nik, @PathVariable(name = "shift") Integer shift ) {
        return pieRepository.getPieData(nik, shift);
    }
}
