package com.loreal.api.api;

import com.loreal.api.model.Idle;
import com.loreal.api.model.LorGetMP;
import com.loreal.api.model.ReturnCode;
import com.loreal.api.model.Type;
import com.loreal.api.repository.BreakdownRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class Breakdown {

    @Autowired
    private BreakdownRepository breakdownRepository;

    @GetMapping(value = "/test",
                produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<LorGetMP> getMp(){
        return breakdownRepository.getMp();
    }


    @GetMapping(value = "/idle/{nik}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<LorGetMP> getIdle(@PathVariable(name = "nik") String nik){
        return breakdownRepository.getIdle(nik);
    }

    @RequestMapping(value = "/push",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ReturnCode insert(@RequestBody Idle idle){
        breakdownRepository.insert(idle);
        return new ReturnCode(0,"Success");
    }

    @RequestMapping(value = "/type",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Type> getType(){
        return breakdownRepository.getType();
    }

    @RequestMapping(value = "/levelDua/{parent}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Type> getLevelDua(@PathVariable(name = "parent") Integer parent){
        return breakdownRepository.getLevelDua(parent);
    }


}
