package com.loreal.api.api;

import com.loreal.api.model.Idle;
import com.loreal.api.model.LorGetMP;
import com.loreal.api.model.ReturnCode;
import com.loreal.api.model.UserOF;
import com.loreal.api.repository.StatusOFRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class StatusOF {

    @Autowired
    private StatusOFRepository statusOFRepository;

    @GetMapping(value = "/status/{wiperOrderNo}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getWeigh(@PathVariable(name = "wiperOrderNo") Double wiperOrderNo) {
        String value = "{\n" +
                "  \"weigh\" :\n" +
                "" + statusOFRepository.getWeigh(wiperOrderNo) +
                ",\n" +
                "  \"pack\" :\n" +
                "" + statusOFRepository.getPack(wiperOrderNo) +
                ",\n" +
                "  \"rm\" : \n" +
                "" + statusOFRepository.getRM(wiperOrderNo) +
                "}";
        return value;
    }

    @GetMapping(value = "/achievement/{nik}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getAchievement(@PathVariable(name = "nik") String nik) {
        String value = "{\n" +
                "  \"individual\" :\n" +
                "" + statusOFRepository.getIndividualAchievement(nik) +
                ",\n" +
                "  \"group\" :\n" +
                "" + statusOFRepository.getGroupAchievement() +
                "}";
        return value;
    }

//    @PostMapping(
//            value = "/update/EndOF",
//            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public ResponseEntity<UserOF> updateEndOF(@RequestBody UserOF userOF){
//        statusOFRepository.updateEndOF(userOF);
//        return new ResponseEntity<UserOF>(userOF, HttpStatus.OK);
////        return new ReturnCode(0,"Success");
//    }

//    @PostMapping(
//            value = "/stop/EndOF",
//            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public ResponseEntity<UserOF> stopEndOF(@RequestBody UserOF userOF){
//        statusOFRepository.stopEndOf(userOF);
//        return new ResponseEntity<UserOF>(userOF, HttpStatus.OK);
////        return new ReturnCode(0,"Success");
//    }

//    @RequestMapping(value = "/update/EndOF",
//            method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public ReturnCode insert(@RequestBody UserOF userOF){
//        statusOFRepository.updateEndOF(userOF);
//        return new ReturnCode(0,"Success");
//    }

    @RequestMapping(value = "/stop/EndOF",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ReturnCode insertStop(@RequestBody UserOF userOF){
        statusOFRepository.stopEndOf(userOF);
        return new ReturnCode(0,"Success");
    }

    @RequestMapping(value = "/update/EndOF",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ReturnCode updateUserOF(@RequestBody UserOF userOF){
        statusOFRepository.updateUserOF(userOF);
        return new ReturnCode(0,"Success");
    }

}
