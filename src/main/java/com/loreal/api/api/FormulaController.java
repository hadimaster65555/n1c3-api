package com.loreal.api.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loreal.api.model.Formula;
import com.loreal.api.repository.FormulaDaoImpl;

@RestController
@RequestMapping("/rest")
public class FormulaController {

	@Autowired
	FormulaDaoImpl formulaDaoImpl;
	
	@RequestMapping(value="/formula/list",produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
	public List<Formula> formulaList() {
		return formulaDaoImpl.getAllFormula();
	}
	
	@RequestMapping(value="/formula/{formula}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
	public Formula getRmQuantityByFormula(@PathVariable("formula") String formula) {
		return formulaDaoImpl.getRmQuantityByFormula(formula);
	}
}
