package com.loreal.api.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loreal.api.model.ReturnCode;
import com.loreal.api.model.UserLoginModel;
import com.loreal.api.repository.UserLoginRepository;

@RestController
@RequestMapping("/rest")
public class UserLoginController {

	@Autowired
	UserLoginRepository userLoginRepository;
	
	@RequestMapping(value="/userlogin/add", consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
	public ReturnCode addLoginData(@RequestBody UserLoginModel userLoginModel) {
		userLoginRepository.insertLoginLogoutData(userLoginModel);
		return new ReturnCode(0,"Success");
	}
		
}
