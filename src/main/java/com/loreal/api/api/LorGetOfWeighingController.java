package com.loreal.api.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loreal.api.model.LorGetOfWeighing;
import com.loreal.api.repository.LorGetOfWeighingDaoImpl;

@RestController
@RequestMapping("/rest")
public class LorGetOfWeighingController {
	
	private static final Logger console = LoggerFactory.getLogger(LorGetOfWeighingController.class);

	@Autowired
	LorGetOfWeighingDaoImpl lorGetOfWeighingDao;

	@RequestMapping(value = "/formulaAndOF/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
	public List<LorGetOfWeighing> listAllFormulaAndOf() {
		return lorGetOfWeighingDao.getAllFormulaAndOF();
	}

	@RequestMapping(value = "/orderquantitybyformula/{productNo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
	public LorGetOfWeighing getOrderQuantityByFormula(@PathVariable("productNo") String productNo) {
		try {
			console.info("parameter product nomor {}", productNo);
			return lorGetOfWeighingDao.getOrderQuantityByFormula(productNo);
		} catch (EmptyResultDataAccessException erdae) {
			erdae.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/formulaAndOf/{wipOrderNo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
	public LorGetOfWeighing getFormulaByOfNumber(@PathVariable("wipOrderNo") Double wipOrderNo) {
		return lorGetOfWeighingDao.getFormulaByOfNumber(wipOrderNo);
	}
}
